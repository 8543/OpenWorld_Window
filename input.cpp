#include "openworld_window.h"
// For input manager
#include <owCore/singletons.h>

CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID)
{
    switch(buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}
// TODO, localise m_CameraHandler into an actual pointer somewhere
// Defining a pointer of this type in Window causes a segfault for
// some reason
using World = ::OpenWorld::World;
#define CAMERA_HANDLER \
    World::getState<OgreBites::SdkCameraMan>("m_CameraHandler")

namespace OpenWorld
{
namespace System
{
// Keyboard
bool Window::keyPressed(const OIS::KeyEvent& arg)
{
    CEGUI::GUIContext& ctx = CEGUI::System::getSingleton().getDefaultGUIContext();
    ctx.injectKeyDown((CEGUI::Key::Scan)arg.key);
    ctx.injectChar((CEGUI::Key::Scan)arg.text);
    InputSingleton::getSingleton().evt_KeyPress.call(m_Keyboard->getAsString(arg.key));

    // Camera
    CAMERA_HANDLER->injectKeyDown(arg);

    return true;
}

bool Window::keyReleased(const OIS::KeyEvent& arg)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp((CEGUI::Key::Scan)arg.key);
    InputSingleton::getSingleton().evt_KeyRelease.call(m_Keyboard->getAsString(arg.key));

    // Camera
    CAMERA_HANDLER->injectKeyUp(arg);

    return true;
}

// Mouse
bool Window::mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
    CEGUI::GUIContext& ctx = CEGUI::System::getSingleton().getDefaultGUIContext();
    ctx.injectMouseButtonDown(convertButton(id));

    // Camera
    CAMERA_HANDLER->injectMouseDown(arg, id);

    return true;
}

bool Window::mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
    CEGUI::GUIContext& ctx = CEGUI::System::getSingleton().getDefaultGUIContext();
    ctx.injectMouseButtonUp(convertButton(id));

    // Camera
    CAMERA_HANDLER->injectMouseUp(arg,id);

    return true;
}

bool Window::mouseMoved(const OIS::MouseEvent& arg)
{
    CEGUI::GUIContext& ctx = CEGUI::System::getSingleton().getDefaultGUIContext();
    ctx.injectMouseMove(arg.state.X.rel, arg.state.Y.rel);

    if(arg.state.Z.rel) {
        ctx.injectMouseWheelChange(arg.state.Z.rel / 120.0f);
    }

    // Camera
    CAMERA_HANDLER->injectMouseMove(arg);

    return true;
}
}
}