#ifndef OPENWORLD_WINDOW
#define OPENWORLD_WINDOW

// OGRE
#include <OGRE/OgreCamera.h>
#include <OGRE/OgreConfigFile.h>
#include <OGRE/OgreEntity.h>
#include <OGRE/OgreRenderWindow.h>
#include <OGRE/OgreRoot.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreString.h>
#include <OGRE/OgreViewport.h>
#include <OGRE/OgreWindowEventUtilities.h>
#include <OGRE/SdkCameraMan.h>

// BULLET
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/btBulletCollisionCommon.h>

// OIS
#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>

// CEGUI
#include <CEGUI/Base.h>
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>

// Window's thread
#include "owCore/openworld_core.h"
#include "owCore/Types/instance.h"
#include "owCore/runner.h"

#include <memory>
#include <functional>
#include <string>

namespace OpenWorld
{
namespace System
{

/*
 * Define properties for use in a world object's representation
 */

struct Mesh {
    std::unique_ptr< Ogre::SceneNode > m_Node;
    std::unique_ptr< Ogre::Entity > m_Entity;
};

struct Physics {
    std::unique_ptr< btCollisionShape > m_Shape;
    std::unique_ptr< btDefaultMotionState > m_Motion;
    std::unique_ptr< btRigidBody > m_RigidBody;
};

class Window : public Ogre::WindowEventListener,
               public Ogre::FrameListener,
               public OIS::KeyListener,
               public OIS::MouseListener
{
   private:
    // System
    Ogre::Root* m_Root;
    Ogre::RenderWindow* m_Window;

    ::OpenWorld::Core::ThreadRunner* m_Thread;
    std::string m_ThreadInitFile;

    OIS::InputManager* m_InputMgr;
    OIS::Mouse* m_Mouse;
    OIS::Keyboard* m_Keyboard;

    // Visual
    Ogre::Camera* m_Camera;
    Ogre::SceneManager* m_SceneMgr;
    Ogre::Viewport* m_Vp;

    Ogre::Light* m_Light;

    // CEGUI
    CEGUI::OgreRenderer* m_Renderer;

    // Resources
    Ogre::String m_Resources;
    Ogre::String m_Plugins;

    // Callback
    std::function<void(void)> m_callback;
    ::OpenWorld::Types::Instance* m_RootInst;

    // Bullet
    btDiscreteDynamicsWorld* m_DynamicsWorld;

    // Helper funcs
    void setupBullet();

   public:
    template <typename F>
    Window(const std::string& title, const std::string& script_path, ::OpenWorld::Types::Instance* root_inst, F&& func)
        : m_Root(0)
        , m_Resources(Ogre::StringUtil::BLANK)
        , m_Plugins(Ogre::StringUtil::BLANK)
        , m_callback(func)
        , m_ThreadInitFile(script_path)
        , m_RootInst(root_inst)
    {
        setup(title);
    }
    ~Window();

    void setup(const std::string& title);
    bool open();

    virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt) override;
    // OIS
    virtual void windowResized(Ogre::RenderWindow* rw) override;
    virtual void windowClosed(Ogre::RenderWindow* rw) override;

    // input.cpp
    virtual bool keyPressed(const OIS::KeyEvent& arg) override;
    virtual bool keyReleased(const OIS::KeyEvent& arg) override;

    virtual bool mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id) override;
    virtual bool mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id) override;

    virtual bool mouseMoved(const OIS::MouseEvent& arg) override;
};

}
}

#endif
