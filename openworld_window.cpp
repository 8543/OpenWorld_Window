#include "openworld_window.h"

#include <iostream>
#include <thread>
#include <owCore/singletons.h>
// Types
#include "Types/window.h"
#include "Types/part.h"

namespace OpenWorld
{
namespace System
{
void Window::setup(const std::string& title)
{
    // Resouce files
    m_Resources = "resources.cfg";
    m_Plugins = "plugins.cfg";

    m_Root = new Ogre::Root(m_Plugins, "window_cfg.cfg", "window_log.log");
//
    // Load
    Ogre::ConfigFile cf;
    cf.load(m_Resources);

    Ogre::String name, locType;
    Ogre::ConfigFile::SectionIterator secIt = cf.getSectionIterator();

    while(secIt.hasMoreElements()) {
        Ogre::ConfigFile::SettingsMultiMap* settings = secIt.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator it;

        for(it = settings->begin(); it != settings->end(); ++it) {
            locType = it->first;
            name = it->second;

            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(name, locType);
        }
    }

    // Render Window
    if(!(m_Root->restoreConfig() || m_Root->showConfigDialog())) {
        return;
    }

    m_Window = m_Root->initialise(true, title.c_str());

    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    // Visual
    m_SceneMgr = m_Root->createSceneManager(Ogre::ST_GENERIC);

    m_Camera = m_SceneMgr->createCamera("RootCam");
    m_Camera->setPosition(30, 10, 0);
    m_Camera->lookAt(0,0,0);
    m_Camera->setNearClipDistance(5);

    m_Vp = m_Window->addViewport(m_Camera);
    m_Vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

    m_Camera->setAspectRatio(Ogre::Real(m_Vp->getActualWidth()) / Ogre::Real(m_Vp->getActualHeight()));

    // Camera handler, put in state
    using World = ::OpenWorld::World;
    auto handler = World::allocateNew<OgreBites::SdkCameraMan>("m_CameraHandler", m_Camera);
    handler->setStyle(OgreBites::CS_ORBIT);
    //

    m_Light = m_SceneMgr->createLight("RootLight");
    m_Light->setPosition(20, 50, 0);
    m_Light->setDirection(-1,-1,0);
    m_Light->setType(Ogre::Light::LT_POINT);
    //m_SceneMgr->setAmbientLight(Ogre::ColourValue(0,0,0));
    m_SceneMgr->setAmbientLight(Ogre::ColourValue(0.5,0.5,0.5));

    // Shadows
    m_SceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    // CEGUI
    m_Renderer = &CEGUI::OgreRenderer::bootstrapSystem();
    CEGUI::ImageManager::setImagesetDefaultResourceGroup("General");
    CEGUI::Font::setDefaultResourceGroup("General");
    CEGUI::Scheme::setDefaultResourceGroup("General");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("General");
    CEGUI::WindowManager::setDefaultResourceGroup("General");

    // Arg 2 is the font size
    CEGUI::FontManager::getSingleton().createFreeTypeFont("DejaVuSans-10", 10, true, "DejaVuSans.ttf", "General");
    CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-10");
    CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");

    // Looks ugly, maybe enable later
    // CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

    // OIS
    Ogre::LogManager::getSingletonPtr()->logMessage("*-*-* OIS Starting");

    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    m_Window->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

// Don't keep hold of the mouse
#if defined OIS_WIN32_PLATFORM
    pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND")));
    pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
    pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
    pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
#elif defined OIS_LINUX_PLATFORM
    pl.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
    pl.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
    pl.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
    pl.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
#endif
    //

    m_InputMgr = OIS::InputManager::createInputSystem(pl);

    // Controls
    m_Keyboard = static_cast<OIS::Keyboard*>(m_InputMgr->createInputObject(OIS::OISKeyboard, true));
    m_Mouse = static_cast<OIS::Mouse*>(m_InputMgr->createInputObject(OIS::OISMouse, true));
    m_Keyboard->setEventCallback(this);
    m_Mouse->setEventCallback(this);

    m_Keyboard->setTextTranslation(OIS::Keyboard::Unicode);

    // Resize
    windowResized(m_Window);

    // Init OIS
    Ogre::WindowEventUtilities::addWindowEventListener(m_Window, this);

    // Fix CEGUI mouse
    CEGUI::GUIContext& ctx = CEGUI::System::getSingleton().getDefaultGUIContext();
    const OIS::MouseState state = m_Mouse->getMouseState();
    ctx.injectMousePosition(state.X.abs, state.Y.abs);

    // Setup Bullet
    setupBullet();

    // Start Lua thread
    m_Thread = new ::OpenWorld::Core::ThreadRunner(m_ThreadInitFile, m_RootInst);

    m_Thread->run<
            Types::Window, Types::Vector3, Types::Part>();
    ::OpenWorld::InputSingleton::register_class(m_Thread->lua);

    m_Root->addFrameListener(this);
    m_Root->startRendering();
}

void Window::setupBullet() {
    using World = ::OpenWorld::World;

    World::setStatePtr("m_SceneMgr", m_SceneMgr);

    auto collision =
            World::allocateStatePtr<btDefaultCollisionConfiguration>("m_CollisionConfig");
    auto broadphase =
            World::allocateStatePtr<btDbvtBroadphase>("m_Broadphase");
    auto dispatcher =
            World::allocateNew<btCollisionDispatcher>("m_Dispatcher", collision);
    // ^ new btCollisionDispatcher(collision)
    auto solver =
            World::allocateStatePtr<btSequentialImpulseConstraintSolver>("m_Solver");

    // Increase locality for step function
    m_DynamicsWorld = World::allocateNew<btDiscreteDynamicsWorld>(
        "m_DynamicsWorld", dispatcher, broadphase, solver, collision);

    btContactSolverInfo& info = m_DynamicsWorld->getSolverInfo();
    info.m_numIterations = 4;

}

// Frame
void Window::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int w, h, d;
    int left, top;

    rw->getMetrics(w, h, d, left, top);

    const OIS::MouseState& ms = m_Mouse->getMouseState();
    ms.width = w;
    ms.height = h;
}

bool Window::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
    if(m_Window->isClosed()) {
        return false;
    }

    m_Keyboard->capture();
    m_Mouse->capture();

    // Panic
    if(m_Keyboard->isKeyDown(OIS::KC_ESCAPE)) {
        return false;
    }

    // Step bullet simulation, 100fps
    m_DynamicsWorld->stepSimulation(1000/100, 4);
    // Camera
    //m_CameraHandler->frameRenderingQueued(evt);
    //
    m_Thread->step();
    m_callback();
    return true;
}

void Window::windowClosed(Ogre::RenderWindow* rw)
{
    if(rw == m_Window) {
        if(m_InputMgr) {
            // Critical TODO
            // Causes seg fault as window closes
            m_InputMgr->destroyInputObject(m_Mouse);
            m_InputMgr->destroyInputObject(m_Keyboard);

            OIS::InputManager::destroyInputSystem(m_InputMgr);
            m_InputMgr = nullptr;
        }
    }
}

bool Window::open()
{
    if(m_Window->isClosed()) {
        return false;
    }//
    if(!m_Root->renderOneFrame()) {
        return false;
    }

    return true;
}

Window::~Window()
{
    Ogre::WindowEventUtilities::removeWindowEventListener(m_Window, this);
    windowClosed(m_Window);
    //delete m_CameraHandler;
    delete m_Root;
}
//--
}
}
