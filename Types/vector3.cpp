#include "vector3.h"

namespace OpenWorld {
namespace Types {

// Default

Vector3::Vector3()
        : x(0), y(0), z(0) {}

Vector3& Vector3::operator=(const Vector3& rhs) {
    x = rhs.x;
    y = rhs.y;
    z = rhs.z;
    return *this;
}

Vector3::Vector3(const Vector3& rhs)
        : x(rhs.x), y(rhs.y), z(rhs.z) {}
Vector3::Vector3(const Vector3&& rhs)
        : x(rhs.x), y(rhs.y), z(rhs.z) {}

Vector3::Vector3(acc_t n_x, acc_t n_y, acc_t n_z)
        : x(n_x), y(n_y), z(n_z) {}

void Vector3::register_class(sol::state_view &state) {
    state.new_usertype<Vector3>(
            "Vector3", sol::constructors<sol::types<float,float,float>>(),
            "x", &Vector3::x,
            "y", &Vector3::y,
            "z", &Vector3::z,

            sol::meta_function::to_string, &Vector3::meta_tostring,
            sol::meta_function::equal_to, &Vector3::operator==
    );
}

// Meta
std::string Vector3::meta_tostring() const {
    std::stringstream ss;
    ss<<x<<","<<y<<","<<z;
    return ss.str();
}

// Convertable to other Vector3 values usable
// Vector3 -> Others

Vector3::operator Ogre::Vector3() const {
    return Ogre::Vector3(x,y,z);
}

Vector3::operator btVector3() const {
    return btVector3(x,y,z);
}

// Others -> Vector3
Vector3::Vector3(const Ogre::Vector3& rhs)
        : x(rhs.x), y(rhs.y), z(rhs.z) {}

Vector3::Vector3(const btVector3& rhs)
        : x(rhs.getX()), y(rhs.getY()), z(rhs.getY()) {}

Vector3::Vector3(const btTransform& rhs)
        : x(rhs.getOrigin().getX()), y(rhs.getOrigin().getY()), z(rhs.getOrigin().getZ()) {}

// Operators
bool Vector3::operator==(const Vector3 &rhs) {
    return (x == rhs.x && y == rhs.y && z == rhs.z);
}

}
}