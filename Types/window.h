#ifndef TYPE_WINDOW_H
#define TYPE_WINDOW_H

#include <memory>

#include <CEGUI/CEGUI.h>
#include <sol.hpp>

#include "owCore/Types/event.h"
#include "owCore/Types/instance.h"

namespace OpenWorld
{
namespace Types
{
    
class Window;
class Button;
class Textbox;
class Label;

namespace Detail
{
template <typename T> class WindowElement
{
   protected:
    T* m_Elem;

   public:
    T* getElement()
    {
        return m_Elem;
    }

    // CEGUI
    void setProperty(std::string key, std::string value);

    // VISIBLE
    bool isVisible();
    void setVisible(bool visible);

    // POSITION
    virtual CEGUI::UVector2 getPosition();
    virtual void setPosition(CEGUI::UVector2 rhs);

    // SIZE
    virtual CEGUI::USize getSize();
    virtual void setSize(CEGUI::USize rhs);

    // TEXT
    virtual void setText(std::string text);
    virtual std::string getText();

    //
    virtual void destroy();

    // EVENTS
    virtual void bindEvents();

    // In built methods
    Event evt_Clicked;
    Event evt_MouseEnter;
    Event evt_MouseLeave;

    bool evt_ClickedFired(const CEGUI::EventArgs& e);
    bool evt_MouseEnterFired(const CEGUI::EventArgs& e);
    bool evt_MouseLeaveFired(const CEGUI::EventArgs& e);
};
}

class Label : public Detail::WindowElement<CEGUI::Window>
{
   public:
    Label(Window* window, std::string name);
    static void register_class(sol::state_view& state);
};

class Button : public Detail::WindowElement<CEGUI::PushButton>
{
   public:
    Button(Window* window, std::string name);
    Button(CEGUI::PushButton* button);
    static void register_class(sol::state_view& state);
};

class Textbox : public Detail::WindowElement<CEGUI::Editbox>
{
   public:
    Textbox(Window* window, std::string name);
    static void register_class(sol::state_view& state);
};

class Window : public Detail::WindowElement<CEGUI::FrameWindow>
{
   private:
    static CEGUI::Window* m_RootWindow;
    Button* m_CloseButton;

   public:
    Window(std::string name);
    static void register_class(sol::state_view& state);

    // Close button
    bool getCloseButtonEnabled();
    void setCloseButtonEnabled(bool value);
    
    // Draggable
    bool getDraggable();
    void setDraggable(bool value);
};

}
}

#endif