#ifndef TYPE_PART_H
#define TYPE_PART_H

#include <sol.hpp>

// Mesh + Physics
#include "owWindow/openworld_window.h"

// Instance + Vector3
#include "owCore/Types/instance.h"
#include "vector3.h"

// TODO More Part constructors for size/position/etc
namespace OpenWorld
{
namespace Types
{
    
class Part : public Instance
{
private:
    // For unique names
    static int m_GID;
    int m_ID;

    // Ogre and Bullet assets
    ::OpenWorld::System::Mesh m_Mesh;
    ::OpenWorld::System::Physics m_Physics;
    //

    /*
     * Expects m_Size, m_Position, m_Grabity, m_Mass to be set
     */
    void buildAssets();

public:
    // Updated per step
    Vector3 m_Size;
    Vector3 m_Position;
    Vector3 m_Gravity;
    float m_Mass;

    // Properties
    Vector3 getGravity();
    void setGravity(Vector3 rhs);

    Vector3 getPosition();
    void setPosition(Vector3 rhs);

    Vector3 getSize();
    void setSize(Vector3 rhs);
    //

    // Methods
    void applyForce(Vector3 force);

    Part(std::string name, Instance* par);
    Part(std::string name);

    // Instance
    virtual void step() override;
    virtual void destroy() override;
    static void register_class(sol::state_view& state);
};

#define TYPE_PART_BIND(T) \
    "Position", sol::property( &T::getPosition, &T::setPosition ),                        \
    "Size", sol::property( &T::getSize, &T::setSize ),                                    \
    "Gravity", sol::property( &T::getGravity, &T::setGravity ),                           \
    "applyForce", &T::applyForce,                                                         \
    sol::base_classes, sol::bases<Instance>()

}
}

#endif