#include "window.h"

namespace OpenWorld
{
namespace Types
{
namespace Detail
{
//----- Window Element -----//
template <typename T> void WindowElement<T>::setProperty(std::string key, std::string value) {
    m_Elem->setProperty(key,value);
}
template <typename T> bool WindowElement<T>::isVisible()
{
    return m_Elem->isVisible();
}
template <typename T> void WindowElement<T>::setVisible(bool visible)
{
    m_Elem->setVisible(visible);
}

// POSITION
template <typename T> CEGUI::UVector2 WindowElement<T>::getPosition()
{
    return m_Elem->getPosition();
}
template <typename T> void WindowElement<T>::setPosition(CEGUI::UVector2 rhs)
{
    m_Elem->setPosition(rhs);
}

// SIZE
template <typename T> CEGUI::USize WindowElement<T>::getSize()
{
    return m_Elem->getSize();
}
template <typename T> void WindowElement<T>::setSize(CEGUI::USize rhs)
{
    m_Elem->setSize(rhs);
}

// TEXT
template <typename T> void WindowElement<T>::setText(std::string text) {
    m_Elem->setText(text);
}
template <typename T> std::string WindowElement<T>::getText() {
    return std::string( m_Elem->getText().c_str() );
}

template <typename T> void WindowElement<T>::destroy() {
     CEGUI::WindowManager::getSingleton().destroyWindow(m_Elem);
 }
 
 // EVENTS
template <typename T> void WindowElement<T>::bindEvents() {
    // Clicked
    m_Elem->subscribeEvent(
    CEGUI::PushButton::EventClicked,
    CEGUI::Event::Subscriber( &WindowElement::evt_ClickedFired, this ) );
        
    // Enter
    m_Elem->subscribeEvent(
    CEGUI::Window::EventMouseEntersArea,
    CEGUI::Event::Subscriber( &WindowElement::evt_MouseEnterFired, this ) );
    
    // Leave
    m_Elem->subscribeEvent(
    CEGUI::Window::EventMouseLeavesArea,
    CEGUI::Event::Subscriber( &WindowElement::evt_MouseLeaveFired, this ) );  
}

template <typename T> bool WindowElement<T>::evt_ClickedFired(const CEGUI::EventArgs& e) {
    evt_Clicked.call();
    return true;
}
template <typename T> bool WindowElement<T>::evt_MouseEnterFired(const CEGUI::EventArgs& e) {
    evt_MouseEnter.call();
    return true;
}
template <typename T> bool WindowElement<T>::evt_MouseLeaveFired(const CEGUI::EventArgs& e) {
    evt_MouseLeave.call();
    return  true;
}
 
}

//----- Window -----//
// Static member
CEGUI::Window* Window::m_RootWindow = nullptr;

Window::Window(std::string name)
{
    CEGUI::WindowManager &wm = CEGUI::WindowManager::getSingleton();
    
    if(m_RootWindow == nullptr) {
        // Singleton
        m_RootWindow = wm.createWindow("DefaultWindow", name);
        CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(m_RootWindow);
    }
    
    m_Elem = static_cast<CEGUI::FrameWindow*>( wm.createWindow("TaharezLook/FrameWindow", name));
    m_RootWindow->addChild(m_Elem);
    
    m_Elem->setSizingEnabled(false);
    
    // Add close button
    CEGUI::PushButton* close = m_Elem->getCloseButton();
    m_CloseButton = new Button(close);
}

void Window::register_class(sol::state_view &state)
{
    // Bind CEGUI literals
    state.new_usertype<CEGUI::UDim>("UDim", sol::constructors<sol::types<float, float>>(),  // ctor { scale, offset }
    "offset", &CEGUI::UDim::d_offset, "scale", &CEGUI::UDim::d_scale,

    sol::meta_function::addition, &CEGUI::UDim::operator+,
    sol::meta_function::subtraction, &CEGUI::UDim::operator-,
    sol::meta_function::equal_to, &CEGUI::UDim::operator==
                                    // sol::meta_function::multiplication, &CEGUI::UDim::operator*
                                    // sol::meta_function::division, &CEGUI::UDim::operator/
                                    );
    state.new_usertype<CEGUI::UVector2>(
        "UVector2", sol::constructors<sol::types<CEGUI::UDim, CEGUI::UDim>>(),  // ctor { UDim(), UDim() }
        "x", &CEGUI::UVector2::d_x,
        "y", &CEGUI::UVector2::d_y,

        sol::meta_function::addition, &CEGUI::UVector2::operator+,
        sol::meta_function::subtraction, &CEGUI::UVector2::operator-,
        sol::meta_function::equal_to, &CEGUI::UVector2::operator==,
        
        // Define our own
        sol::meta_function::division, [](const CEGUI::UVector2& rhs) {
            // Divide
            CEGUI::UDim x = CEGUI::UDim(rhs.d_x.d_scale/2, rhs.d_x.d_offset/2);
            CEGUI::UDim y = CEGUI::UDim(rhs.d_y.d_scale/2, rhs.d_y.d_offset/2);
            return CEGUI::UVector2(x,y);
        }
        
        );
    state.new_usertype<CEGUI::USize>(
        "USize", sol::constructors<sol::types<CEGUI::UDim, CEGUI::UDim>>(),  // ctor { UDim(), UDim() }
        "width", &CEGUI::USize::d_width,
        "height", &CEGUI::USize::d_height,

        sol::meta_function::addition, &CEGUI::USize::operator+,
        sol::meta_function::subtraction, &CEGUI::USize::operator-,
        sol::meta_function::equal_to, &CEGUI::USize::operator==,
        
        // Define our own
        sol::meta_function::division, [](const CEGUI::USize& rhs) {
            CEGUI::UDim x = CEGUI::UDim(rhs.d_width.d_scale/2, rhs.d_width.d_offset/2);
            CEGUI::UDim y = CEGUI::UDim(rhs.d_height.d_scale/2, rhs.d_height.d_offset/2);
            return CEGUI::USize(x,y);
        }
        
        );
    //
    state.new_usertype<Window>(
        "Window", sol::constructors<sol::types<std::string>>(),
        "destroy", &Window::destroy,
        "Visible", sol::property(&Window::isVisible, &Window::setVisible),
        "CloseButtonEnabled", sol::property(&Window::getCloseButtonEnabled, &Window::setCloseButtonEnabled),
        "Text", sol::property(&Window::getText, &Window::setText),
        "Position", sol::property(&Window::getPosition, &Window::setPosition), 
        "Size", sol::property(&Window::getSize, &Window::setSize),
        "Draggable", sol::property(&Window::getDraggable, &Window::setDraggable),
        
        "CloseButton", &Window::m_CloseButton
        
        );
        
    // Init children
    Button::register_class(state);
    Textbox::register_class(state);
    Label::register_class(state);
}

bool Window::getCloseButtonEnabled() {
    return m_Elem->isCloseButtonEnabled();
}
void Window::setCloseButtonEnabled(bool value) {
    m_Elem->setCloseButtonEnabled(value);
}

// Dragging
// DRAGGABLE
bool Window::getDraggable()
{
    return m_Elem->isDragMovingEnabled();
}
void Window::setDraggable(bool value)
{
    m_Elem->setDragMovingEnabled(value);
}

//------ Button -----//
Button::Button(Window* window, std::string name) {
    m_Elem = static_cast<CEGUI::PushButton*>( CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", name));
    window->getElement()->addChild(m_Elem);
    
    bindEvents();
}
Button::Button(CEGUI::PushButton* button) {
    m_Elem = button;
    
    bindEvents();
}

void Button::register_class(sol::state_view& state) {
    state.new_usertype<Button>(
        "Button", sol::constructors< sol::types<Window*, std::string> >(),
        "destroy", &Button::destroy,
        
        "Visible", sol::property(&Button::isVisible, &Button::setVisible),
        "Text", sol::property(&Button::getText, &Button::setText),
        "Position", sol::property(&Button::getPosition, &Button::setPosition), 
        "Size", sol::property(&Button::getSize, &Button::setSize),
        "setProperty", &Button::setProperty,
        
        "Clicked", &Button::evt_Clicked,
        "MouseEnter", &Button::evt_MouseEnter,
        "MouseLeave", &Button::evt_MouseLeave
    );
}
//------ Label  ------//

Label::Label(Window* window, std::string name) {
    m_Elem = static_cast<CEGUI::Window*>( CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticText", name));
    window->getElement()->addChild(m_Elem);
    
    bindEvents();
}

void Label::register_class(sol::state_view& state) {
    state.new_usertype<Label>(
        "Label", sol::constructors< sol::types<Window*, std::string> >(),
        "destroy", &Label::destroy,
        
        "Visible", sol::property(&Label::isVisible, &Label::setVisible),
        "Text", sol::property(&Label::getText, &Label::setText),
        "Position", sol::property(&Label::getPosition, &Label::setPosition), 
        "Size", sol::property(&Label::getSize, &Label::setSize),
        "setProperty", &Label::setProperty,
        
        "Clicked", &Label::evt_Clicked,
        "MouseEnter", &Label::evt_MouseEnter,
        "MouseLeave", &Label::evt_MouseLeave
    );
}

//------ Textbox ------//
Textbox::Textbox(Window* window, std::string name) {
    m_Elem = static_cast<CEGUI::Editbox*>( CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Editbox", name));
    window->getElement()->addChild(m_Elem);
    
    bindEvents();
}
void Textbox::register_class(sol::state_view& state) {
    state.new_usertype<Textbox>(
        "Textbox", sol::constructors< sol::types<Window*, std::string> >(),
        "destroy", &Textbox::destroy,
        
        "Visible", sol::property(&Textbox::isVisible, &Textbox::setVisible),
        "Text", sol::property(&Textbox::getText, &Textbox::setText),
        "Position", sol::property(&Textbox::getPosition, &Textbox::setPosition), 
        "Size", sol::property(&Textbox::getSize, &Textbox::setSize),
        "setProperty", &Textbox::setProperty,
        
        "Clicked", &Textbox::evt_Clicked,
        "MouseEnter", &Textbox::evt_MouseEnter,
        "MouseLeave", &Textbox::evt_MouseLeave
    );
}

}
}
