#include "part.h"

namespace OpenWorld {
namespace Types {

// ctor
Part::Part(std::string name, Instance* par)
        : Instance(name, par) {
    m_Size = Vector3(100,2,100);
    m_Position = Vector3(0,0,0);
    m_Gravity = Vector3(0,-1,0);
    m_Mass = 1;
    buildAssets();
}

Part::Part(std::string name)
        : Instance(name) {
    m_Size = Vector3(100,2,100);
    m_Position = Vector3(0,0,0);
    m_Gravity = Vector3(0,-1,0);
    m_Mass = 1;
    buildAssets();
}

void Part::register_class(sol::state_view& state) {
    state.new_usertype<Part>(
            "Part", sol::constructors< sol::types<std::string>, sol::types<std::string,Instance*> >(),
            TYPE_PART_BIND(Part),
            TYPE_INSTANCE_BIND(Part)
    );
}

// Part globals
int Part::m_GID = 0;

using World = ::OpenWorld::World;
void Part::buildAssets() {

    m_ID = Part::m_GID;
    Part::m_GID++;

    //----- Physics //

    // Get the dynamics world from World's state
    auto m_DynamicsWorld = World::getState<btDynamicsWorld>("m_DynamicsWorld");
    auto m_SceneMgr = World::getState<Ogre::SceneManager>("m_SceneMgr");

    if(m_DynamicsWorld == nullptr || m_SceneMgr == nullptr) {
        throw std::runtime_error("Part asset builder attempted to run before state was consistent");
    }

    if(m_Physics.m_RigidBody.get() != nullptr) {
        // Remove from world
        m_DynamicsWorld->removeRigidBody(m_Physics.m_RigidBody.get());
    }

    m_Physics.m_Shape.reset(
            new btBoxShape(m_Size)
    );
    m_Physics.m_Motion.reset(
            new btDefaultMotionState(
                    btTransform(btQuaternion(0,0,0,1),
                    m_Position)
            )
    );

    btVector3 fallInertia; // (0,0,0)
    m_Physics.m_Shape->calculateLocalInertia(m_Mass, fallInertia);
    btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(
            m_Mass, m_Physics.m_Motion.get(), m_Physics.m_Shape.get(), fallInertia
    );

    m_Physics.m_RigidBody.reset(
            new btRigidBody(fallRigidBodyCI)
    );
    m_Physics.m_RigidBody->setGravity(m_Gravity);
    m_Physics.m_RigidBody->setActivationState(DISABLE_DEACTIVATION);
    m_Physics.m_RigidBody->setActivationState(ACTIVE_TAG);

    m_DynamicsWorld->addRigidBody(m_Physics.m_RigidBody.get());

    //----- Ogre //
    // If already exists, dissasemble it
    if(m_Mesh.m_Entity.get() != nullptr) {
        m_Mesh.m_Node->detachObject(m_Mesh.m_Entity.get());
    }

    m_Mesh.m_Entity.reset( m_SceneMgr->createEntity("Cube2.mesh") );
    m_Mesh.m_Entity->setCastShadows(true);

    std::stringstream ss;
    ss<<"PartNode"<<m_ID;
    std::string node_name = ss.str();

    m_Mesh.m_Node.reset(
            m_SceneMgr->getRootSceneNode()->createChildSceneNode(node_name)
    );
    m_Mesh.m_Node->attachObject(m_Mesh.m_Entity.get());
    m_Mesh.m_Node->setScale(m_Size);
    m_Mesh.m_Node->setPosition(m_Position);
    m_Mesh.m_Node->setScale(m_Size);
}

// Override from Instance
void Part::step() {
    if(m_Mesh.m_Entity == nullptr || m_Mesh.m_Node == nullptr) {
        return;
    }
    btTransform transform = m_Physics.m_RigidBody->getWorldTransform();
    m_Mesh.m_Node->setPosition(Vector3(transform));

    btQuaternion rot(transform.getRotation());
    m_Mesh.m_Node->setOrientation(
            Ogre::Quaternion(
                    rot.getW(),
                    rot.getX(),
                    rot.getY(),
                    rot.getZ()
            )
    );
    m_Position = Vector3(transform);
}

void Part::destroy() {
    Instance::destroy();

    // Remove physics body, remove node
    auto m_DynamicsWorld = World::getState<btDynamicsWorld>("m_DynamicsWorld");
    m_DynamicsWorld->removeRigidBody(m_Physics.m_RigidBody.get());

    m_Mesh.m_Node->detachAllObjects();
}

// Properties
Vector3 Part::getGravity() {
    return m_Physics.m_RigidBody->getGravity();
}
void Part::setGravity(Vector3 rhs) {
    m_Gravity = rhs;
    if(rhs == Vector3(0,0,0)) {
        m_Mass = 0;
    } else {
        m_Mass = 1;
    }
    buildAssets();
}

Vector3 Part::getPosition() {
    btTransform pos = m_Physics.m_RigidBody->getWorldTransform();
    return pos;
}
void Part::setPosition(Vector3 rhs) {
    m_Position = rhs;
    buildAssets();
}

Vector3 Part::getSize() {
    return m_Size;
}
void Part::setSize(Vector3 rhs) {
    m_Size = rhs;
    buildAssets();
}

// Methods
void Part::applyForce(Vector3 force) {
    m_Physics.m_RigidBody->applyCentralImpulse(force);
}


}
}