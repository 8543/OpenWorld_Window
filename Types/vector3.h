#ifndef WINDOW_VECTOR3_H
#define WINDOW_VECTOR3_H

#include <sol.hpp>

#include <OGRE/OgreVector3.h>
#include <bullet/LinearMath/btVector3.h>
#include <bullet/LinearMath/btTransform.h>

// TODO Vector3 operators
namespace OpenWorld {
namespace Types {

struct Vector3 final {
public:
    using acc_t = float;

    acc_t x;
    acc_t y;
    acc_t z;

    Vector3();
    Vector3& operator=(const Vector3& rhs);
    Vector3(const Vector3& rhs);
    Vector3(const Vector3&& rhs);
    Vector3(acc_t n_x, acc_t n_y, acc_t n_z);

    // Convertable to other Vector3 values usable
    // Vector3 -> Others
    operator Ogre::Vector3() const;
    operator btVector3() const;

    // Others -> Vector3
    Vector3(const Ogre::Vector3& rhs);
    Vector3(const btVector3& rhs);
    Vector3(const btTransform& rhs);

    // Operators
    bool operator==(const Vector3& rhs);

    // Meta
    std::string meta_tostring() const;

    static void register_class(sol::state_view& state);
};

}
}

#endif //WINDOW_VECTOR3_H
