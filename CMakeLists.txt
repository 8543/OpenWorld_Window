cmake_minimum_required(VERSION 3.5)
project(Window)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -fPIC -shared")

set(SOURCE_FILES
        openworld_window.cpp
        input.cpp
        Types/part.cpp
        Types/window.cpp
        Types/vector3.cpp
        )

include_directories(~/ClionProjects/ /usr/include/bullet)
link_directories(/usr/lib ~/ClionProjects/owCore/ ~/ClionProjects/owWindow/ )

add_library(Window SHARED ${SOURCE_FILES})